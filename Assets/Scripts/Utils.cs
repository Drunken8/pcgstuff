﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{

    public static T[][] InitArray<T>(Vector2Int size)
    {
        T[][] ret = new T[size.x][];
        for (int i = 0; i < size.x; i++)
        {
            ret[i] = new T[size.y];
        }
        return ret;
    }

    public static Vector2Int NextVector2Int(int min, int max)
    {
        return new Vector2Int(Random.Range(min, max), Random.Range(min, max));
    }



    public static int[][] InitArrayRandom(Vector2Int size, int min, int max)
    {
        int[][] ret = new int[size.x][];
        for (int i = 0; i < size.x; i++)
        {
            ret[i] = new int[size.y];
            for (int j = 0; j < size.y; j++)
            {
                ret[i][j] = Random.Range(min, max);
            }
        }
        return ret;
    }

    public static T[][] InitArray<T>(Vector2Int size, T defaultVal)
    {
        T[][] ret = new T[size.x][];
        for (int i = 0; i < size.x; i++)
        {
            ret[i] = new T[size.y];
            for (int j = 0; j < size.y; j++)
            {
                ret[i][j] = defaultVal;
            }
        }
        return ret;
    }

    public static Dictionary<T, K> InitDictionary<T,K>(IEnumerable<T> keys, K defaultValue)
    {
        Dictionary<T, K> map = new Dictionary<T, K>();
        foreach (T key in keys)
        {
            map.Add(key, defaultValue);
        }
        return map;
    }

    public static IEnumerable<Vector2Int> Enumerate2DField(Vector2Int size)
    {
        for (int i = 0; i < size.x; i++)
        {
            for (int j = 0; j < size.y; j++)
            {
                yield return new Vector2Int(i, j);
            }
        }
    }

    //extensions
    public static T getTiltedClockwise<T>(this T[][] me, int count, Vector2Int pos)
    {
        return me.getValue(pos.Tilted(count, me.GetLength(0)-1));
    }

    public static Vector2Int Tilted(this Vector2Int me, int count, int max)
    {
        count = count % 4;
        Vector2Int tilted = me;
        for (int i = 0; i < count; i++)
        {
            tilted.x = max - tilted.y;
            tilted.y = tilted.x;
        }
        return tilted;
    }

    public static void Mutate(this Vector2Int me, int min, int max)
    {
        if (Random.Range(0,1f)<0.5f)
        {
            me.x = Mathf.Min(Mathf.Max(me.x + Random.Range(-5,5),min), max);
        }
        else
        {
            me.y = Mathf.Min(Mathf.Max(me.y + Random.Range(-5, 5), min), max);
        }
    }

    public static Vector2Int ToVector2Int(this Vector2 me)
    {
        return new Vector2Int((int)me.x, (int)me.y);
    }

    public static List<ListDungeonIndividual.DungeonObject> Copy(this List<ListDungeonIndividual.DungeonObject> me)
    {
        List<ListDungeonIndividual.DungeonObject> cp = new List<ListDungeonIndividual.DungeonObject>();
        foreach (var item in me)
        {
            cp.Add(item.Copy());
        }
        return cp;
    }

    public static T getValue<T>(this T[][] me, Vector2Int pos)
    {
        return me[pos.x][pos.y];
    }

    public static Vector2Int getSizeVector<T>(this T[][] me)
    {
        return new Vector2Int(me.GetLength(0), me[0].GetLength(0));
    }

    public static T[][] Copy<T>(this T[][] me){
        T[][] cp = new T[me.GetLength(0)][];
        for (int i = 0; i < me.GetLength(0); i++)
        {
            cp[i] = new T[me[i].GetLength(0)];
            for (int j = 0; j < me[i].GetLength(0); j++)
            {
                cp[i][j] = me[i][j];
            }
        }
        return cp;
    }

    public static void SaveSet<T>(this T[][] me,int x, int y, T val)
    {
        if (x >= 0 && x <= me.GetLength(0) &&
            y >= 0 && y <= me[0].GetLength(0))
        {
            me[x][y] = val;
        }
    }

    public static int Count<T>(this T[][] me, T counted)
    {
        int c = 0;
        for (int i = 0; i < me.GetLength(0); i++)
        {
            for (int j = 0; j < me[0].GetLength(0); j++)
            {
                if (me[i][j].Equals(counted))
                {
                    c++;
                }
            }
        }
        return c;
    }

    public static T min<T>(this Dictionary<T, float> me, T defaultValue)
    {
        float cMin = float.MaxValue;
        T best = defaultValue;
        foreach (KeyValuePair<T,float> item in me)
        {
            if (item.Value < cMin)
            {
                cMin = item.Value;
                best = item.Key;
            }
        }
        return best;
    }
    
}
