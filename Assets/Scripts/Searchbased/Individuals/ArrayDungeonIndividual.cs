﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ArrayDungeonIndividual : IIndividual
{
    protected static int GlobalID = 0;

    protected readonly int ID;
    protected readonly int Generation;
    protected float score = float.MinValue;

    public Vector2Int size;
    protected Tile[][] Dungeon;

    public ArrayDungeonIndividual(Vector2Int size, Vector2Int start, Vector2Int exit)
    {
        Dungeon = Utils.InitArray<Tile>(size);
        this.size = size;
        this.ID = GlobalID++;
        this.Generation = 1;

        Dungeon[start.x][start.y] = Tile.Start;
        Dungeon[exit.x][exit.y] = Tile.Exit;
    }

    public ArrayDungeonIndividual(Tile[][] Dungeon, int gen)
    {
        this.Dungeon = Dungeon;
        this.size = new Vector2Int(Dungeon.GetLength(0), Dungeon[0].GetLength(0));
        this.Generation = gen;
        this.ID = GlobalID++;
    }

    public virtual IIndividual Clone()
    {
        return new ArrayDungeonIndividual(Dungeon.Copy(), Generation+1);
    }

    public float Evaluate()
    {
        score = 0;
        List<Vector2Int> PoIs = Utils.Enumerate2DField(size).Where((vec) => (int)GetDungeon()[vec.x][vec.y] > 1).ToList();
        Vector2Int start = new Vector2Int(0,0);
        Vector2Int exit = new Vector2Int(0, 0);
        try
        {
            start = PoIs.First((vec) => GetDungeon()[vec.x][vec.y] == Tile.Start);
        }
        catch (System.Exception)
        {
            do
            {
                start.x = Random.Range(0, size.x-1);
                start.x = Random.Range(0, size.y-1);
            } while (Dungeon[start.x][start.y] == Tile.Exit);

            Dungeon[start.x][start.y] = Tile.Start;
            PoIs = Utils.Enumerate2DField(size).Where((vec) => (int)GetDungeon()[vec.x][vec.y] > 1).ToList();
        }
        try
        {
            exit = PoIs.First((vec) => GetDungeon()[vec.x][vec.y] == Tile.Exit);
        }
        catch (System.Exception)
        {
            do
            {
                exit.x = Random.Range(0, size.x - 1);
                exit.x = Random.Range(0, size.y - 1);
            } while (Dungeon[exit.x][exit.y] == Tile.Start);

            Dungeon[exit.x][exit.y] = Tile.Exit;
            PoIs = Utils.Enumerate2DField(size).Where((vec) => (int)GetDungeon()[vec.x][vec.y] > 1).ToList();
        }
        //Vector2Int start = PoIs.First((vec) => GetDungeon()[vec.x][vec.y] == Tile.Start);
        //Vector2Int exit = PoIs.First((vec) => GetDungeon()[vec.x][vec.y] == Tile.Exit);

        Dictionary<Tile, float> costDict = new Dictionary<Tile, float>();
        costDict.Add(Tile.None, 1);
        costDict.Add(Tile.Wall, float.PositiveInfinity);
        costDict.Add(Tile.Start, 1);
        costDict.Add(Tile.Exit, 1);
        costDict.Add(Tile.Monster, 4);
        costDict.Add(Tile.Treasure, 1);

        score = AStar.GetShortestPath<Tile>(start, exit, GetDungeon(), costDict).Count;

        float reachable = 0;
        if (score > 0)
        {

            foreach (Vector2Int pi in PoIs)
            {
                if (AStar.GetShortestPath<Tile>(start, exit, GetDungeon(), costDict).Count > 0)
                {
                    reachable += 1;
                }
            }
            score *= reachable / PoIs.Count;
        }

        // TODO erreichbarkeit, linearität, (minimal)encounters
        return score;
    }

    public float GetGeneration()
    {
        return Generation;
    }

    public float GetID()
    {
        return ID;
    }

    public float GetScore()
    {
        return score;
    }

    public virtual void Identify()
    {
        Debug.Log("ArrayDungeon GEN" + Generation + ": " + ID +
            " Score: " + score);
    }

    public virtual void Kill()
    {
        
    }

    private void MovingMutatioion(float randNum, Tile Test, Tile Set, Vector2Int mutationPoint, bool overR)
    {
        if (randNum > 0.75f)
        {
            if (mutationPoint.x+1<size.x && ( Dungeon[mutationPoint.x + 1][mutationPoint.y] == Test || overR))
            {
                Dungeon.SaveSet(mutationPoint.x + 1, mutationPoint.y, Set);
            }
        }
        else if (randNum > 0.5f)
        {
            if (mutationPoint.x - 1 >=0 &&(Dungeon[mutationPoint.x - 1][mutationPoint.y] == Test || overR))
            {
                Dungeon.SaveSet(mutationPoint.x - 1, mutationPoint.y, Set);
            }
        }
        else if (randNum > 0.25f)
        {
            if (mutationPoint.y + 1 < size.y &&(Dungeon[mutationPoint.x][mutationPoint.y + 1] == Test || overR))
            {
                Dungeon.SaveSet(mutationPoint.x, mutationPoint.y + 1, Set);
            }
        }
        else
        {
            if (mutationPoint.y -1 >= 0 && (Dungeon[mutationPoint.x][mutationPoint.y - 1] == Test || overR))
            {
                Dungeon.SaveSet(mutationPoint.x, mutationPoint.y - 1, Set);
            }
        }
    }

    public virtual void Mutate()
    {
        int mutationCount =(int)( 0.05f*( Dungeon.GetLength(0)* Dungeon[0].GetLength(0)));
        for (int i = 0; i < mutationCount; i++)
        {
            Vector2Int mutationPoint = new Vector2Int(Random.Range(0,size.x), Random.Range(0, size.y));
            float randNum = Random.Range(0, 1f);

            switch (Dungeon[mutationPoint.x][mutationPoint.y])
            {
                case Tile.None:
                    //spawn wall, monster or treasure
                    if (randNum > 0.2f)
                    {
                        Dungeon.SaveSet(mutationPoint.x, mutationPoint.y, Tile.Wall);
                    }
                    else if(randNum > 0.1f)
                    {
                        Dungeon.SaveSet(mutationPoint.x, mutationPoint.y, Tile.Monster);
                    }
                    else
                    {
                        Dungeon.SaveSet(mutationPoint.x, mutationPoint.y, Tile.Treasure);
                    }
                    break;
                case Tile.Wall:
                    //remove or extend in a direction
                    if (randNum > 0.5f)
                    {
                        Dungeon.SaveSet(mutationPoint.x, mutationPoint.y, Tile.None);
                    } else
                    {
                        MovingMutatioion(randNum*2, Tile.None, Tile.Wall, mutationPoint, false);
                    }
                    break;
                case Tile.Start:
                    //move
                    MovingMutatioion(randNum, Tile.None, Tile.Start, mutationPoint, true);
                    if (Dungeon.Count(Tile.Start) > 1)
                    {
                        Dungeon.SaveSet(mutationPoint.x, mutationPoint.y, Tile.None);
                    }
                    break;
                case Tile.Exit:
                    //move
                    MovingMutatioion(randNum, Tile.None, Tile.Exit, mutationPoint, true);
                    if (Dungeon.Count(Tile.Exit) > 1)
                    {
                        Dungeon.SaveSet(mutationPoint.x, mutationPoint.y, Tile.None);
                    }
                    break;
                case Tile.Monster:
                    //move or remove
                    Dungeon.SaveSet(mutationPoint.x, mutationPoint.y, Tile.None);
                    if (randNum < 0.5)
                    {
                        MovingMutatioion(randNum * 2, Tile.None, Tile.Monster, mutationPoint, false);
                    }
                    break;
                case Tile.Treasure:
                    //move or remove
                    Dungeon.SaveSet(mutationPoint.x, mutationPoint.y, Tile.None);
                    if (randNum < 0.5)
                    {
                        MovingMutatioion(randNum * 2, Tile.None, Tile.Treasure, mutationPoint, false);
                    }
                    break;
                default:
                    throw new System.Exception("Can not mutate " + Dungeon[mutationPoint.x][mutationPoint.y].ToString());
            }
        }
    }


    public virtual Tile[][] GetDungeon()
    {
        return Dungeon;
    }

    public void visit(IIndividualVisitor vis)
    {
        vis.Report(this);
    }
}

public enum Tile
{
    None,
    Wall,
    Start,
    Exit,
    Monster,
    Treasure
}
