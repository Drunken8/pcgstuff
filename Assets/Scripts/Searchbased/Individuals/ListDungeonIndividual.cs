﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ListDungeonIndividual : ArrayDungeonIndividual
{
    private List<DungeonObject> DungeonList;
    Vector2Int start, exit;

    public ListDungeonIndividual(Vector2Int size, Vector2Int start, Vector2Int exit) : base(size, start, exit)
    {
        DungeonList = new List<DungeonObject>
        {
            new PoI(start, Tile.Start),
            new PoI(exit, Tile.Exit)
        };
        this.start = start;
        this.exit = exit;
    }

    private ListDungeonIndividual(Tile[][] Dungeon, int gen, List<DungeonObject> dungeonObjects) : base(Dungeon, gen)
    {
        this.DungeonList = dungeonObjects;
    }
    
    public override void Identify()
    {
        base.Identify();
    }

    public override IIndividual Clone()
    {
        return new ListDungeonIndividual(Dungeon, Generation + 1, DungeonList.Copy());
    }

    public override void Kill()
    {
        
    }

    public override void Mutate()
    {
        int mutationCount = 10;

        for (int i = 0; i < mutationCount; i++)
        {
            float randomValue = Random.Range(0, 1.0f);
            int mutationPoint = Random.Range(0, DungeonList.Count - 1);
            if (randomValue<0.1)//delete
            {
                if (mutationPoint > 1) //Do not delete start or end
                {
                    DungeonList.RemoveAt(mutationPoint);
                }
            }
            else if(randomValue<0.5) //add
            {
                if (randomValue < 0.3)
                {
                    DungeonList.Add(new Wall(Utils.NextVector2Int(0, size.x), Utils.NextVector2Int(0, size.x)));
                } else if(randomValue < 0.4)
                {
                    DungeonList.Add(new PoI(Utils.NextVector2Int(0, size.x), Tile.Treasure));
                }
                else
                {
                    DungeonList.Add(new PoI(Utils.NextVector2Int(0, size.x), Tile.Monster));
                }
            }
            else //mutate
            {
                DungeonList[mutationPoint].Mutate(size.x);
            }
            
        }
    }

    public override Tile[][] GetDungeon()
    {
        Dungeon = Utils.InitArray<Tile>(size);
        
        foreach (DungeonObject item in DungeonList.Skip(0).Reverse())
        {
            foreach (KeyValuePair<Vector2Int, Tile> info in item.EnumerateTiles())
            {
                Dungeon[info.Key.x][info.Key.y] = info.Value;
            }
        }
        
        return Dungeon;
    }
    



    //Utils
    public abstract class DungeonObject
    {
        public abstract void Mutate(int max);
        public abstract IEnumerable<KeyValuePair<Vector2Int, Tile>> EnumerateTiles();
        public abstract DungeonObject Copy();
    }

    private class Wall : DungeonObject
    {
        Vector2Int start;
        Vector2Int end;

        public Wall(Vector2Int start, Vector2Int end)
        {
            this.start = start;
            this.end = end;
        }

        public override DungeonObject Copy()
        {
            return new Wall(start, end);
        }

        public override IEnumerable<KeyValuePair<Vector2Int, Tile>> EnumerateTiles()
        {
            Vector2 vec = end - start;
            Vector2 dir = vec.normalized;
            float sampleWidth = 0.7f;
            Vector2Int oldpos = new Vector2Int(-1,-1);
            Vector2 curr = start;

            for (float i = 0; i < vec.magnitude; i+=sampleWidth)
            {
                curr += dir * sampleWidth;
                if (!curr.ToVector2Int().Equals(oldpos))
                {
                    oldpos = curr.ToVector2Int();
                    yield return new KeyValuePair<Vector2Int, Tile>(curr.ToVector2Int(), Tile.Wall);
                }
            }

            if (!oldpos.Equals(end)) //final sample may be not at end, especially with coarser sampling
            {
                yield return new KeyValuePair<Vector2Int, Tile>(end, Tile.Wall);
            }
            
        }

        public override void Mutate(int max)
        {
            if (Random.Range(0,1f)> 0.5f)
            {
                start.Mutate(0, max);
            }
            else
            {
                end.Mutate(0, max);
            }
        }
    }

    private class PoI : DungeonObject
    {
        Vector2Int pos;
        Tile tile;

        public PoI(Vector2Int pos, Tile type)
        {
            this.pos = pos;
            tile = type;
        }

        public override DungeonObject Copy()
        {
            return new PoI(pos, tile);
        }

        public override IEnumerable<KeyValuePair<Vector2Int, Tile>> EnumerateTiles()
        {
            yield return new KeyValuePair<Vector2Int, Tile>(pos, tile);
        }

        public override void Mutate(int max)
        {
            pos.Mutate(0,max);
        }
    }
}
