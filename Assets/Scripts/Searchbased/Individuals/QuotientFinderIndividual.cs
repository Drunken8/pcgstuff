﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuotientFinderIndividual : IIndividual
{
    static int globalID = 0; //never Acessed inb pallel.... i hope


    private Vector2Int me;
    private readonly int generation;
    private readonly int ID;
    private float score = float.MinValue;
    private readonly float target;

    public QuotientFinderIndividual(Vector2Int me, int generation, float target)
    {
        this.me = me;
        this.generation = generation;
        this.ID = globalID++;
        this.target = target;
        Debug.Log("NumberIndividual: " +
            GetID() + " gen" +
            GetGeneration() + " is born"
            );
    }

    public IIndividual Clone()
    {
        Debug.Log("NumberIndividual: " +
            GetID() + " gen" +
            GetGeneration() + " is cloned"
            );
        return new QuotientFinderIndividual(me, generation+1, target);
    }

    public float Evaluate()
    {
        score = Mathf.Abs(((float)me.x / (float)me.y) - target) * -1; //negative scores

        Debug.Log("NumberIndividual: " +
            GetID() + " gen" +
            GetGeneration() + " got score: " + 
            GetScore()
            );
        return score;
    }

    public float GetGeneration()
    {
        return generation;
    }

    public float GetID()
    {
        return ID;
    }

    public float GetScore()
    {
        return score;
    }

    public void Kill()
    {
        Debug.Log("NumberIndividual: " +
            GetID() + " gen" +
            GetGeneration() + " died trying " +
            me + " Scored: " +
            GetScore()
            );
    }

    public void Mutate()
    {
        me += new Vector2Int(Random.Range(-1, 1), Random.Range(-1, 1));
        Debug.Log("NumberIndividual: " +
            GetID() + " gen" +
            GetGeneration() + " Mutated to " +
            me
            );
    }

    public void Identify()
    {
        Debug.Log(">>>NumberIndividual: " +
           GetID() + " gen" +
           GetGeneration() + " lives with " +
           me + " Scored: " +
           GetScore()
           );
    }

    public void visit(IIndividualVisitor vis)
    {
        vis.Report(this);
    }
}
