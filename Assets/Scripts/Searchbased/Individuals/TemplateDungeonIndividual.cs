﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemplateDungeonIndividual : ArrayDungeonIndividual
{
    private int[][] DungeonTemplated; //indexes of Template
    private int[][] DungeonTemplatedRotations; //number of rotations clockwise
    private List<Tile[][]> Templates;
    private readonly Vector2Int Start, Exit;

    public TemplateDungeonIndividual(Vector2Int size, List<Tile[][]> templates, Vector2Int start, Vector2Int exit, Vector2Int templateSize) : base(size,start, exit)
    {
        Vector2Int effectiveSize = new Vector2Int(size.x / templateSize.x, size.y / templateSize.y);

        DungeonTemplated = Utils.InitArrayRandom(effectiveSize,-1,templates.Count-1);
        DungeonTemplatedRotations = Utils.InitArrayRandom(effectiveSize,0,3);
        Templates = templates;

        Start = start;
        Exit = exit;
    }

    public TemplateDungeonIndividual(Tile[][] Dungeon, int gen, List<Tile[][]> templates, int[][] dungeonTemplated, int[][] dungeonRotations, Vector2Int start, Vector2Int exit) : base(Dungeon, gen)
    {
        DungeonTemplated = dungeonTemplated;
        DungeonTemplatedRotations = dungeonRotations;
        Templates = templates;

        Start = start;
        Exit = exit;
    }


    public override IIndividual Clone()
    {
        return new TemplateDungeonIndividual(Dungeon.Copy(), Generation+1, Templates, DungeonTemplated.Copy(), DungeonTemplatedRotations.Copy(), Start, Exit);
    }

    public override void Identify()
    {
        base.Identify();
    }

    public override void Kill()
    {
        
    }

    public override void Mutate()
    {
        int mutationCount = (int) (DungeonTemplated.GetLength(0) * DungeonTemplated.GetLength(0) * 0.15f);
        for (int i = 0; i < mutationCount; i++)
        {
            float randomValue = Random.Range(0, 1.0f);
            Vector2Int mutationPoint = new Vector2Int(Random.Range(0, DungeonTemplated.GetLength(0)), Random.Range(0, DungeonTemplated[0].GetLength(0)));

            if (randomValue < 0.5) //"Spawn" a new Template, despawns if -1
            {
                DungeonTemplated[mutationPoint.x][mutationPoint.y] = (int) (randomValue * 2 * Templates.Count) - 1;
            }
            else //Rotate the template
            {
                if (randomValue < 0.75) //rot left
                {
                    DungeonTemplatedRotations[mutationPoint.x][mutationPoint.y] = DungeonTemplatedRotations[mutationPoint.x][mutationPoint.y] - 1 % 4;
                } else //rot right
                {
                    DungeonTemplatedRotations[mutationPoint.x][mutationPoint.y] = DungeonTemplatedRotations[mutationPoint.x][mutationPoint.y] + 1 % 4;
                }
            }

        }
    }

    public override Tile[][] GetDungeon()
    {
        Dungeon = Utils.InitArray<Tile>(size);

        for (int i = 0; i < DungeonTemplated.GetLength(0); i++)
        {
            for (int j = 0; j < DungeonTemplated[0].GetLength(0); j++)
            {
                if (DungeonTemplated[i][j] != -1)
                {
                    Tile[][] current = Templates[DungeonTemplated[i][j]];

                    for (int x = 0; x < current.GetLength(0); x++)
                    {
                        for (int y = 0; y < current[0].GetLength(0); y++)
                        {
                            Dungeon[x + i * current.GetLength(0)][y + j * current[0].GetLength(0)] = 
                                current.getTiltedClockwise(DungeonTemplatedRotations[i][j], new Vector2Int(x, y));
                        }
                    }
                }
            }
        }

        Dungeon[Start.x][Start.y] = Tile.Start;
        Dungeon[Exit.x][Exit.y] = Tile.Exit;

        return Dungeon;
    }

}
