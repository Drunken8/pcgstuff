﻿public interface IIndividual
{
    //stats
    //return calculated Score
    float GetScore();
    //return birthgeneration
    float GetGeneration();
    //return individual ID
    float GetID();


    //calculate score
    float Evaluate();
    //clean up/ give info on death
    void Kill();
    //replicate self, increment generation of newly born
    IIndividual Clone();
    //mutate self
    void Mutate();
    //give info on console/ sceen
    void Identify();

    void visit(IIndividualVisitor vis);
}
