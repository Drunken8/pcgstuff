﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Darwin : MonoBehaviour
{
    //configuration
    [Header("Setup Configuration")]
    public IndividualTypes type;
    public bool ShowDebug = true;
    public bool doShuffle = false;
    public bool Pause = false;
    [Header("Training Configuration")]
    [Tooltip("Portion of Individuals reproducing each Generation")]
    [Range(0f, 1f)]
    public float survivalRate = 0.5f;
    [Tooltip("Portion of empty places given to completely new Species")]
    [Range(0f, 1f)]
    public float newcomerRate = 0.1f;
    public int populationSize = 20;
    public int EndGeneration = 100;
    [Tooltip("Number of Generations per frame, set bigger than EndGeneration to use one Frame")]
    public int BatchSize = 1;

    //used internally
    private List<IIndividual> population = new List<IIndividual>();
    private int generation = 0;
    private Visualizer vis;


    // Start is called before the first frame update
    void Start()
    {
        Populate(null, type);
        vis = gameObject.GetComponent<Visualizer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!Pause)
        {
            int numThisFrame = 0;
            while (generation < EndGeneration && numThisFrame < BatchSize)
            {
                //maintain counters for this frame and overall
                generation++;
                numThisFrame++;

                //start algorithm
                if (doShuffle)
                {
                    Shuffle();
                }
                Evaluate();
                Sort();
                Kill();
                Clone();
                Mutate();

                //show info
                Debug.Log("===============GEN" + generation + "===============");
                if (ShowDebug)
                {
                    printState();
                }

                if (generation >= EndGeneration) //will be executed only once because while will exit later
                {
                    //get best:
                    Evaluate();
                    Sort();
                    IIndividual best = population[0];
                    Debug.Log("AND THE WINNER IS: ");
                    best.Identify();
                }
            }

            vis.VisualizeIndividual(population[0]);
        }
    }
    

    private void printState()
    {
        string s = "SCORES: ";
        foreach (IIndividual item in population)
        {
            s += item.GetScore() + " ";
            item.Identify();
        }
        Debug.Log(s);
    }


    //make the population, max populationSize many, make it possible to add predefinded
    private void Populate(List<IIndividual> premade, IndividualTypes type)
    {
        if (premade != null)
        {
            population.AddRange(premade);
        }
        while (population.Count < populationSize)
        {
            population.Add(IndividualFactory.GetIndividual(type));
        }
    }

    //shuffle individuals to escape loss of gradient
    private void Shuffle()
    {
        for (int i = 0; i < population.Count; i++)
        {
            int nPos = UnityEngine.Random.Range(0, population.Count-1);
            IIndividual tmp = population[i];
            population[i] = population[nPos];
            population[nPos] = tmp;
        }
    } 

    //check each individual how fit it is
    private void Evaluate()
    {
        foreach (IIndividual individual in population)
        {
            individual.Evaluate();
        }
    }

    //sort individuals by score
    private void Sort()
    {
        population.Sort((IIndividual x, IIndividual y) => y.GetScore().CompareTo(x.GetScore()));
    }

    //kill (1-survivalrate)% worst individuals
    private void Kill()
    {
        //remove reversed to preserve numbering
        for (int i = populationSize-1; i > (int)(populationSize * survivalRate); i--)
        {
            population[i].Kill();
            population.RemoveAt(i);
        }
    }

    //make offsprings of fittest individuals
    private void Clone()
    {
        int fitIndividuals = (int)(populationSize * survivalRate);
        int newcommerPlaces = (int)((populationSize - fitIndividuals) * newcomerRate);
        int counter = 0;
        

        while (population.Count < populationSize-newcommerPlaces)
        {
            population.Add(population[counter].Clone());
            counter = (counter + 1) % fitIndividuals; //avoid offsprings of new untested generation
        }

        //fill left places with newcommers
        for (int i = 0; i < newcommerPlaces; i++)
        {
            population.Add(IndividualFactory.GetIndividual(type));
        }
        
    }

    //mutate new offsprings
    private void Mutate()
    {
        for (int i = (int)(populationSize*survivalRate)+1; i < populationSize; i++)
        {
            population[i].Mutate();
        }
    }
}
