﻿using System.Collections.Generic;
using UnityEngine;
public static class IndividualFactory
{

    static readonly int[][] wall = new int[][]
    {
        new int[]{ 0,0,0,0,1,0,0,0,0},
        new int[]{ 0,0,0,0,1,0,0,0,0},
        new int[]{ 0,0,0,0,1,0,0,0,0},
        new int[]{ 0,0,0,0,1,0,0,0,0},
        new int[]{ 0,0,0,0,1,0,0,0,0},
        new int[]{ 0,0,0,0,1,0,0,0,0},
        new int[]{ 0,0,0,0,1,0,0,0,0},
        new int[]{ 0,0,0,0,1,0,0,0,0},
        new int[]{ 0,0,0,0,1,0,0,0,0},
    };
    static readonly int[][] blocks = new int[][]
    {
        new int[]{ 0,0,0,0,0,0,0,0,0},
        new int[]{ 0,0,1,0,0,0,0,0,0},
        new int[]{ 0,1,1,1,0,0,0,0,0},
        new int[]{ 0,0,1,1,0,0,0,0,1},
        new int[]{ 0,0,0,0,0,0,0,1,1},
        new int[]{ 0,0,1,0,0,0,0,0,1},
        new int[]{ 0,0,1,1,0,0,0,0,0},
        new int[]{ 0,0,0,1,1,0,0,0,0},
        new int[]{ 0,0,0,1,1,0,0,0,0},
    };
    static readonly int[][] room = new int[][]
    {
        new int[]{ 1,1,0,0,0,0,0,0,0},
        new int[]{ 1,1,1,1,0,1,1,1,1},
        new int[]{ 1,1,0,0,0,0,1,1,0},
        new int[]{ 0,1,0,0,0,0,0,1,0},
        new int[]{ 0,1,0,0,0,0,0,1,0},
        new int[]{ 0,1,0,0,0,0,0,1,1},
        new int[]{ 0,1,0,0,0,0,0,1,1},
        new int[]{ 0,1,1,1,0,1,1,1,0},
        new int[]{ 0,0,1,1,0,0,0,0,0},
    };
    static readonly int[][] pillars = new int[][]
    {
        new int[]{ 0,0,0,0,0,0,0,0,0},
        new int[]{ 0,1,1,0,0,0,1,1,0},
        new int[]{ 0,1,1,0,0,0,1,1,0},
        new int[]{ 0,0,0,0,0,0,0,0,0},
        new int[]{ 0,0,0,0,0,0,0,0,0},
        new int[]{ 0,0,0,0,0,0,0,0,0},
        new int[]{ 0,1,1,0,0,0,1,1,0},
        new int[]{ 0,1,1,0,0,0,1,1,0},
        new int[]{ 0,0,0,0,0,0,0,0,0},
    };
    static readonly int[][] cavepart1 = new int[][]
    {
        new int[]{ 1,1,1,1,1,1,1,1,1},
        new int[]{ 1,1,1,1,1,1,1,1,1},
        new int[]{ 1,1,0,0,0,0,0,1,0},
        new int[]{ 1,1,0,0,0,0,0,0,0},
        new int[]{ 1,1,0,0,0,0,0,0,0},
        new int[]{ 1,0,0,0,0,0,1,0,0},
        new int[]{ 1,0,0,0,0,0,1,1,0},
        new int[]{ 1,0,0,0,0,0,0,0,0},
        new int[]{ 1,0,0,0,1,0,0,0,0},
    };
    static readonly int[][] cavepart2 = new int[][]
   {
        new int[]{ 1,1,1,0,0,0,0,1,1},
        new int[]{ 1,1,1,0,0,1,1,1,1},
        new int[]{ 1,1,0,0,0,0,0,1,0},
        new int[]{ 0,0,0,0,0,0,0,0,0},
        new int[]{ 1,1,0,0,0,0,0,0,1},
        new int[]{ 1,0,0,0,0,0,0,0,0},
        new int[]{ 1,0,0,1,0,0,0,0,0},
        new int[]{ 1,0,0,0,0,0,0,0,0},
        new int[]{ 1,0,0,0,0,0,0,0,0},
   };
    static readonly int[][] cavepart3 = new int[][]
  {
        new int[]{ 1,1,1,0,1,1,0,1,1},
        new int[]{ 1,1,1,0,0,1,1,1,1},
        new int[]{ 1,1,0,0,0,0,0,1,1},
        new int[]{ 0,0,0,0,0,0,0,0,1},
        new int[]{ 0,0,0,1,1,0,0,0,1},
        new int[]{ 1,0,0,0,1,0,0,0,0},
        new int[]{ 1,1,0,1,0,0,0,0,0},
        new int[]{ 1,1,0,0,0,0,0,0,1},
        new int[]{ 1,0,0,0,0,0,0,1,1},
  };
    static readonly int[][] cavepart4 = new int[][]
 {
        new int[]{ 1,1,1,0,1,1,0,1,1},
        new int[]{ 1,1,1,0,1,1,1,1,1},
        new int[]{ 1,1,0,0,1,0,0,1,1},
        new int[]{ 0,0,0,0,1,0,0,0,1},
        new int[]{ 0,0,0,1,1,0,0,0,1},
        new int[]{ 1,0,0,0,1,1,0,0,0},
        new int[]{ 1,1,0,1,1,0,0,0,0},
        new int[]{ 1,1,0,0,1,1,0,0,1},
        new int[]{ 1,0,0,0,0,1,0,1,1},
 };
    static readonly int[][] cavepart5 = new int[][]
{
        new int[]{ 0,0,0,1,1,1,0,1,1},
        new int[]{ 0,0,0,1,1,1,1,1,1},
        new int[]{ 0,0,0,0,1,0,0,1,1},
        new int[]{ 0,0,0,0,1,0,0,0,1},
        new int[]{ 0,0,0,1,1,0,0,0,1},
        new int[]{ 0,0,0,0,1,1,0,0,1},
        new int[]{ 0,0,0,1,1,0,0,0,0},
        new int[]{ 0,0,0,0,1,1,0,0,1},
        new int[]{ 0,0,0,0,0,1,0,1,1},
};
    static readonly int[][] tunnel = new int[][]
    {
        new int[]{ 1,0,0,0,0,0,1,0,1},
        new int[]{ 1,1,0,0,0,0,1,0,1},
        new int[]{ 1,1,0,0,0,0,0,0,1},
        new int[]{ 1,0,0,0,0,0,1,1,1},
        new int[]{ 1,0,0,0,0,0,0,1,1},
        new int[]{ 1,0,0,1,1,0,0,0,1},
        new int[]{ 1,0,0,0,0,0,0,0,1},
        new int[]{ 1,1,0,0,0,0,0,0,1},
        new int[]{ 1,0,0,0,0,0,0,0,1},
    };
    

    public static IIndividual GetIndividual(IndividualTypes type)
    {
        int dim = 15;
        Vector2Int start = new Vector2Int(0, 0);
        Vector2Int end = new Vector2Int(0, 0);

        switch (type)
        {
            case IndividualTypes.QuotientFinder:
                return new QuotientFinderIndividual(new Vector2Int(Random.Range(1, 1000), Random.Range(1, 1000)), 1, 1f);// (1+Mathf.Sqrt(5))/2);
            case IndividualTypes.ArrayDungeon:
                dim = 30;
                while (start.Equals(end))
                {
                    start.x = Random.Range(0, dim - 1);
                    start.y = Random.Range(0, dim - 1);
                    end.x = Random.Range(0, dim - 1);
                    end.y = Random.Range(0, dim - 1);
                }
                return new ArrayDungeonIndividual(new Vector2Int(dim, dim), start, end);
            case IndividualTypes.TemplateDungeon:
                dim = 9*3;
                while (start.Equals(end))
                {
                    start.x = Random.Range(0, dim - 1);
                    start.y = Random.Range(0, dim - 1);
                    end.x = Random.Range(0, dim - 1);
                    end.y = Random.Range(0, dim - 1);
                }
                return new TemplateDungeonIndividual(new Vector2Int(dim, dim), GenerateTemplates(), start,end, new Vector2Int(9,9));
            case IndividualTypes.ListDungeon:
                dim = 30;
                while (start.Equals(end))
                {
                    start.x = Random.Range(0, dim - 1);
                    start.y = Random.Range(0, dim - 1);
                    end.x = Random.Range(0, dim - 1);
                    end.y = Random.Range(0, dim - 1);
                }
                return new ListDungeonIndividual(new Vector2Int(dim, dim), start, end);
            default:
                throw new System.Exception("Not supported Individual Kind");
        }
    }

    private static List<Tile[][]> GenerateTemplates()
    {
        List<Tile[][]> templates = new List<Tile[][]>();
        //templates.Add(ToTileMap(wall));
        //templates.Add(ToTileMap(blocks));
        templates.Add(ToTileMap(room));
        templates.Add(ToTileMap(pillars));
        templates.Add(ToTileMap(cavepart1));
        templates.Add(ToTileMap(cavepart2));
        templates.Add(ToTileMap(cavepart3));
        templates.Add(ToTileMap(cavepart4));
        templates.Add(ToTileMap(cavepart5));
        templates.Add(ToTileMap(tunnel));
        return templates;
    }

    private static Tile[][] ToTileMap(int[][] orig)
    {
        Tile[][] map = Utils.InitArray<Tile>(orig.getSizeVector<int>());
        for (int i = 0; i < map.getSizeVector().x; i++)
        {
            for (int j = 0; j < map.getSizeVector().y; j++)
            {
                map[i][j] = (Tile)orig[i][j];
            }
        }
        return map;
    }
}



public enum IndividualTypes
{
    QuotientFinder,
    ArrayDungeon,
    TemplateDungeon,
    ListDungeon
}
