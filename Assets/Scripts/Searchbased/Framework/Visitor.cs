﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IIndividualVisitor
{
    void Report(ArrayDungeonIndividual ind);
    void Report(QuotientFinderIndividual ind);
}
