﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Visualizer : MonoBehaviour, IIndividualVisitor
{

    [Header("ArrayDungeon")]
    public bool render = true;
    public GameObject GridPrefab;
    public UnityEngine.Tilemaps.TileBase FreeTile;
    public UnityEngine.Tilemaps.TileBase WallTile;
    public UnityEngine.Tilemaps.TileBase EnterTile;
    public UnityEngine.Tilemaps.TileBase ExitTile;
    public UnityEngine.Tilemaps.TileBase MonsterTile;
    public UnityEngine.Tilemaps.TileBase TreasureTile;
    
    private GameObject Grid;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void VisualizeIndividual(IIndividual individual)
    {
        individual.visit(this);
    }

    public void Report(ArrayDungeonIndividual ind)
    {
        if (Grid == null)
        {
            Grid = GameObject.Instantiate(GridPrefab);
        }

        Tile[][] map = ind.GetDungeon();
        UnityEngine.Tilemaps.Tilemap tMap = Grid.GetComponentInChildren<UnityEngine.Tilemaps.Tilemap>();
        //tMap.BoxFill(Vector3Int.zero, baseTile, 0, 0, map.GetLength(0), map[0].GetLength(0));

        foreach (Vector2Int pos in Utils.Enumerate2DField(new Vector2Int(map.GetLength(0), map[0].GetLength(0))))
        {
            switch (map[pos.x][pos.y])
            {
                case Tile.None:
                    tMap.SetTile(new Vector3Int(pos.x, pos.y, 0), FreeTile);
                    break;
                case Tile.Wall:
                    tMap.SetTile(new Vector3Int(pos.x, pos.y, 0), WallTile);
                    break;
                case Tile.Start:
                    tMap.SetTile(new Vector3Int(pos.x, pos.y, 0), EnterTile);
                    break;
                case Tile.Exit:
                    tMap.SetTile(new Vector3Int(pos.x, pos.y, 0), ExitTile);
                    break;
                case Tile.Monster:
                    tMap.SetTile(new Vector3Int(pos.x, pos.y, 0), MonsterTile);
                    break;
                case Tile.Treasure:
                    tMap.SetTile(new Vector3Int(pos.x, pos.y, 0), TreasureTile);
                    break;
                default:
                    break;
            }
        }
        //tMap.RefreshAllTiles();
    }

    public void Report(QuotientFinderIndividual ind)
    {
        throw new System.NotImplementedException();
    }
}
