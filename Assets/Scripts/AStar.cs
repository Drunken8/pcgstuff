﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class AStar
{
   private static float h(Vector2Int from, Vector2Int to)
    {
        return (from - to).magnitude;
    }

    private static IEnumerable<Vector2Int> Neighbours<T>(Vector2Int center, int height, int width, T[][] map, Dictionary<T, float> costDict)
    {
        if (center.y + 1 < height && !float.IsPositiveInfinity( costDict[map.getValue(center + Vector2Int.up)] )) yield return center + Vector2Int.up;
        if (center.y + -1 >= 0 && !float.IsPositiveInfinity(costDict[map.getValue(center + Vector2Int.down)])) yield return center + Vector2Int.down;
        if (center.x + 1 < width && !float.IsPositiveInfinity(costDict[map.getValue(center + Vector2Int.right)])) yield return center + Vector2Int.right;
        if (center.x - 1 >= 0 && !float.IsPositiveInfinity(costDict[map.getValue(center + Vector2Int.left)])) yield return center + Vector2Int.left;
    }

    private static List<Vector2Int> ReconstructPath(Dictionary<Vector2Int,Vector2Int> cameFrom, Vector2Int current)
    {
        List<Vector2Int> path = new List<Vector2Int>();
        path.Add(current);
        while (cameFrom.ContainsKey(current))
        {
            current = cameFrom[current];
            path.Add(current);
        }
        path.Reverse();
        return path;
    }

    private static Vector2Int GetMinFScoreInOpenSet(List<Vector2Int> openSet, Dictionary<Vector2Int,float> fScore)
    {
        Vector2Int minVec = openSet[0];
        float minVal = float.MaxValue;
        foreach (Vector2Int vec in openSet)
        {
            if (fScore[vec]<minVal)
            {
                minVal = fScore[vec];
                minVec = vec;
            }
        }
        return minVec;
    }

    public static List<Vector2Int> GetShortestPath<T>(Vector2Int start, Vector2Int goal, T[][] map, Dictionary<T,float> costDict)
    {
        // The set of discovered nodes that need to be (re-)expanded.
        // Initially, only the start node is known.
        List<Vector2Int> openSet = new List<Vector2Int>();
        List<Vector2Int> closedSet = new List<Vector2Int>();
        openSet.Add(start);
        
        // For node n, cameFrom[n] is the node immediately preceding it on the cheapest path from start to n currently known.
        Dictionary<Vector2Int, Vector2Int> cameFrom = new Dictionary<Vector2Int, Vector2Int>();

        // For node n, gScore[n] is the cost of the cheapest path from start to n currently known.
        Dictionary<Vector2Int, float> gScore = Utils.InitDictionary<Vector2Int, float>(Utils.Enumerate2DField(new Vector2Int(map.GetLength(0), map[0].GetLength(0))), float.MaxValue); //map with default value of Infinity
        gScore[start] = 0;

        // For node n, fScore[n] := gScore[n] + h(n).
        Dictionary<Vector2Int, float> fScore = Utils.InitDictionary<Vector2Int, float>(Utils.Enumerate2DField(new Vector2Int(map.GetLength(0), map[0].GetLength(0))), float.MaxValue);
        fScore[start] = h(start, goal);

        while (openSet.Count != 0)
        {
            Vector2Int current = GetMinFScoreInOpenSet(openSet, fScore); //the node in openSet having the lowest fScore[] value
            if (current.Equals(goal))
            {
                return ReconstructPath(cameFrom, current);
            }
            openSet.Remove(current);
            closedSet.Add(current);

            foreach (Vector2Int neighbor in Neighbours(current, map[0].GetLength(0), map.GetLength(0),map,costDict))
            {
                if (closedSet.Contains(neighbor))
                {
                    continue;
                }
                // d(current,neighbor) is the weight of the edge from current to neighbor
                // tentative_gScore is the distance from start to the neighbor through current
                float tentativeGScore = gScore[current] + costDict[map[neighbor.x][neighbor.y]];
                if (!openSet.Contains(neighbor))
                {
                    openSet.Add(neighbor);
                }
                if (tentativeGScore < gScore[neighbor])
                {
                    // This path to neighbor is better than any previous one. Record it!
                    cameFrom[neighbor] = current;
                    gScore[neighbor] = tentativeGScore;
                    fScore[neighbor] = gScore[neighbor] + h(neighbor, goal);
                }
            }
        }
        // Open set is empty but goal was never reached
        return new List<Vector2Int>();
    }
}
